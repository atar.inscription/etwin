# In-memory Hammerfest service

`HammerfestService` in-memory implementation: all the data is stored in RAM.

It is intended for local development only in situations where communicating with the official server is undesirable.
