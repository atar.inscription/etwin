export interface Like {
  url: string;
  likes: number;
  title: string;
}
